import React from "react";
import { Share, View, Button, Linking } from "react-native";
import * as Sharing from "expo-sharing";
import * as FileSystem from "expo-file-system";
const URL = "https://flewber.com";
const MESSAGE =
  "Simply Private - Private Flights Priced Right Worldwide LUXE private jet charter service with on-demand regional flights. Download the App Now 🛩";
const APP = () => {
  const onShare = async () => {
    try {
      const result = await Share.share(
        {
          title: "Test Title",
          message: MESSAGE.concat("\n", URL),
        },
        {
          //ios support only
          excludedActivityTypes: ["com.apple.UIKit.activity.PostToFacebook"],
        }
      );
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  // const onLinkToInstagram = async () => {
  //   const instagramURL = `https://www.instagram.com/create/story`;
  //   // const uri = await getGalleryImageAsync();
  //   // const encodedURL = encodeURIComponent(uri);
  //   // const instagramURL = `instagram://library?AssetPath=${encodedURL}`;

  //   return Linking.openURL(instagramURL);
  // };
  const onLinkToFacebook = () => {
    let facebookParameters = [];
    if (URL) facebookParameters.push("u=" + encodeURI(URL));

    if (MESSAGE) facebookParameters.push("quote=" + encodeURI(MESSAGE));
    const url =
      "https://www.facebook.com/sharer/sharer.php?" +
      facebookParameters.join("&");
    return Linking.openURL(url);
  };
  const initiateWhatsApp = () => {
    const msg = MESSAGE.concat("\n", URL);

    let url = `https://wa.me/?text=${encodeURI(msg)}`;

    return Linking.openURL(url);
  };
  const tweetNow = () => {
    let twitterParameters = [];
    const twitterViaAccount = "FlyFlewber";
    if (URL) twitterParameters.push("url=" + encodeURI(URL));
    if (MESSAGE) twitterParameters.push("text=" + encodeURI(MESSAGE));
    if (twitterViaAccount)
      twitterParameters.push("via=" + encodeURI(twitterViaAccount));
    const url =
      "https://twitter.com/intent/tweet?" + twitterParameters.join("&");
    return Linking.openURL(url);
  };
   const imageShare = () => {
     const image_source = "https://flewberdev.com/assets/images/login_1.jpg";

    FileSystem.downloadAsync(
      image_source,
      FileSystem.documentDirectory + ".jpeg"
    )
      .then(({ uri }) => {
        console.log("Finished downloading to ", uri);
        Sharing.shareAsync(uri);
      })
      .catch((error) => {
        console.error(error);
      });
  };
  return (
    <>
      <View style={{ marginTop: 50 }}>
        <Button onPress={onShare} title="Share" />
      </View>
      {/* <View style={{ marginTop: 30 }}>
        <Button onPress={onLinkToInstagram} title="Instagram Share" />
      </View> */}
      <View style={{ marginTop: 30 }}>
        <Button onPress={onLinkToFacebook} title="Facebook Share" />
      </View>
      <View style={{ marginTop: 30 }}>
        <Button onPress={initiateWhatsApp} title="Whatsapp Share" />
      </View>
      <View style={{ marginTop: 30 }}>
        <Button onPress={tweetNow} title="Twitter Share" />
      </View>
        <View style={{ marginTop: 30 }}>
        <Button onPress={imageShare} title="Image Share" />
      </View>
    </>
  );
};

export default APP;
