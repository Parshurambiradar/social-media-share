import * as React from "react";
import { Text, View, StyleSheet, TouchableOpacity, Image } from "react-native";
import * as Sharing from "expo-sharing";
import * as FileSystem from "expo-file-system";
const image_source = "https://flewberdev.com/assets/images/login_1.jpg";

const ImageShare = () => {
  const share = () => {
    FileSystem.downloadAsync(
      image_source,
      FileSystem.documentDirectory + ".jpeg"
    )
      .then(({ uri }) => {
        console.log("Finished downloading to ", uri);
        Sharing.shareAsync(uri);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <View style={styles.container}>
      <Image
        style={{ width: 300, height: 300, margin: 15 }}
        source={{ uri: image_source }}
      />

      <TouchableOpacity style={styles.button} onPress={share}>
        <Text style={styles.buttonText}>Share</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",

    backgroundColor: "#ecf0f1",
    padding: 8,
    alignItems: "center",
  },
  button: {
    backgroundColor: "red",
    padding: 15,
  },
  buttonText: {
    color: "white",
  },
});

export default ImageShare;
